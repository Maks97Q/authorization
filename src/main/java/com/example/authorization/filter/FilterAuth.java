package com.example.authorization.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

//@WebFilter(urlPatterns = "/ProfileServlet")
public class FilterAuth implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        PrintWriter writer = response.getWriter();
        String name = (String) request.getSession().getAttribute("name");

        if (name == null) {
            writer.print("first, log in");
        } else {
            request.getRequestDispatcher("ProfileServlet").include(request, response);
        }

    }
}
