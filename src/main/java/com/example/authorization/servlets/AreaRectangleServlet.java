package com.example.authorization.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/AreaRectangleServlet")
public class AreaRectangleServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        request.getRequestDispatcher("link.jsp").include(request, response);
        int width = Integer.parseInt(request.getParameter("width"));
        int length = Integer.parseInt(request.getParameter("length"));

        if (width > 0 && length > 0) {
            out.print("Area of a rectangle = " + width * length);
        } else {
            out.print("wrong number format");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        request.getRequestDispatcher("link.jsp").include(request, response);
        PrintWriter out = response.getWriter();
        String name = (String) request.getSession().getAttribute("name");
        if (name == null) {
            out.print("Please login first to app");
        } else {
            request.getRequestDispatcher("square.jsp").include(request, response);
        }
    }
}
