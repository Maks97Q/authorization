package com.example.authorization.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        request.getRequestDispatcher("link.jsp").include(request, response);
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String name1 = (String) request.getSession().getAttribute("name");

        if (name1 == null) {
            if (password.equals("12345") && name.equals("maks")) {
                out.print("Welcome, " + name);
                request.getSession().setAttribute("name", name);
            } else {
                out.print("Sorry, username or password error!");
                request.getRequestDispatcher("login.jsp").include(request, response);
            }
        } else {
            out.print("you have been logged in");
        }
        out.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("link.jsp").include(request, response);
        request.getRequestDispatcher("login.jsp").include(request, response);
    }
}
