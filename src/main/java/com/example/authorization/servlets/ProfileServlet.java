package com.example.authorization.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ProfileServlet")
public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        request.getRequestDispatcher("link.jsp").include(request, response);
        String name = (String) request.getSession(false).getAttribute("name");

        if (name != null) {
            out.print("Hello, " + name + " you entered your profile");
        } else {
            out.print("Please login first to app");
            request.getRequestDispatcher("login.jsp").include(request, response);
        }
        out.close();
    }
}
